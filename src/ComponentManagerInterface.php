<?php

namespace Drupal\pdb_twig;

/**
 * Defines an interface for a component manager.
 */
interface ComponentManagerInterface {

  /**
   * Builds a twig component for rendering.
   *
   * @param string $component
   *   The component identifier.
   * @param array $config
   *   The component config.
   *
   * @return array
   *   The component build info.
   */
  public function build($component, array $config);

}
