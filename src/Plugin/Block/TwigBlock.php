<?php

namespace Drupal\pdb_twig\Plugin\Block;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\pdb\Plugin\Block\PdbBlock;
use Drupal\pdb_twig\Event\PdbTwigPrePreRenderEvent;
use Drupal\pdb_twig\Event\PdbTwigPreRenderAlterEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Exposes a Twig component as a block.
 *
 * @Block(
 *   id = "twig_component",
 *   admin_label = @Translation("Twig component"),
 *   deriver = "\Drupal\pdb_twig\Plugin\Derivative\TwigBlockDeriver"
 * )
 */
class TwigBlock extends PdbBlock implements TrustedCallbackInterface {

  /**
   * Provides safe namespacing for pdb twig themes.
   */
  const THEME_PREFIX = 'pdb_twig_';

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Component class instance.
   *
   * @var object
   */
  protected $classInstance;

  /**
   * Constructs a new TwigBlock.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UuidInterface $uuid, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $uuid);

    $this->eventDispatcher = $event_dispatcher;

    $this->classInstance = $this->getClassInstance();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uuid'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();

    $info = $this->getComponentInfo();

    if ($this->classInstance && method_exists($this->classInstance, 'build')) {
      $build = $this->classInstance->build($build, $this->configuration);

      // Allow a block to not render by returning an empty array.
      if (empty($build)) {
        return [];
      }
    }

    if (isset($info['theme'])) {
      if (is_array($info['theme'])) {
        $theme = self::THEME_PREFIX . $info['theme']['template'];
      }
      else {
        // This adds support to use existing themes.
        $theme = $info['theme'];
      }

      $build['#theme'] = $theme;
    }

    if (isset($info['cache'])) {
      $build['#cache'] = $this->processCache($info);
    }

    $build['#pre_render'] = [[$this, 'preRender']];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function attachLibraries(array $component) {
    $parent_libraries = parent::attachLibraries($component);

    // Wrap parent libraries into the expected `library` property.
    $libraries = [
      'library' => $parent_libraries,
    ];

    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if ($this->classInstance && method_exists($this->classInstance, 'buildConfigurationForm')) {
      $pdb_configuration = $this->configuration['pdb_configuration'] ?? [];
      $form = $this->classInstance->buildConfigurationForm($form, $form_state, $pdb_configuration);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if ($this->classInstance && method_exists($this->classInstance, 'submitConfigurationForm')) {
      $this->classInstance->submitConfigurationForm($form, $form_state);

      $this->configuration['pdb_configuration'] = $form_state->getValue('pdb_configuration');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getJsContexts(array $contexts) {
    // Return empty array to avoid not needed extra processing as
    // the contexts will not be needed on client side.
    return [];
  }

  /**
   * Process Component block cache metadata.
   *
   * @param array $info
   *   The twig block info array.
   *
   * @return array
   *   The cache array.
   */
  protected function processCache(array $info) {
    $cache_config = $info['cache'];
    $cache = $this->getCacheDefaults();

    // Static metadata from Component info.yml file.
    if (isset($cache_config['keys'])) {
      $cache['keys'] = $cache_config['keys'];
    }

    if (isset($cache_config['contexts'])) {
      $cache['contexts'] = $cache_config['contexts'];
    }

    if (isset($cache_config['tags'])) {
      $cache['tags'] = Cache::mergeTags($cache['tags'], $cache_config['tags']);
    }

    if (isset($cache_config['max-age'])) {
      $cache['max-age'] = $cache_config['max-age'];
    }

    // Dynamic metadata from Component class instance or contexts.
    if ($this->classInstance) {
      if (method_exists($this->classInstance, 'getCacheKeys')) {
        $instance_keys = $this->classInstance->getCacheKeys($this->configuration);

        if (count($instance_keys)) {
          $cache['keys'] = array_merge($cache['keys'], $instance_keys);
        }
      }

      if (method_exists($this->classInstance, 'getCacheContexts')) {
        $instance_contexts = $this->classInstance->getCacheContexts($this->configuration);

        if (count($instance_contexts)) {
          $cache['contexts'] = Cache::mergeContexts($cache['contexts'], $instance_contexts);
        }
      }

      if (method_exists($this->classInstance, 'getCacheTags')) {
        $instance_tags = $this->classInstance->getCacheTags($this->configuration);

        if (count($instance_tags)) {
          $cache['tags'] = Cache::mergeTags($cache['tags'], $instance_tags);
        }
      }
    }

    // Add support for components requiring contexts.
    // These should expose context's tags and contexts cache metadata.
    // In some special cases the related context cache metadata is not needed
    // and needs to be skipped, use `skip_required_contexts` for that.
    $skip_required_contexts = $cache_config['skip_required_contexts'] ?? FALSE;
    if (isset($this->configuration['contexts']) && !$skip_required_contexts) {
      foreach ($this->configuration['contexts'] as $context) {
        // Merge tags and contexts when possible.
        if ($context instanceof CacheableDependencyInterface) {
          $cache['tags'] = Cache::mergeTags($cache['tags'], $context->getCacheTags());
          $cache['contexts'] = Cache::mergeContexts($cache['contexts'], $context->getCacheContexts());
        }
      }
    }

    return $cache;
  }

  /**
   * Pre-render callback for the Component block.
   *
   * @param array $build
   *   The component build array.
   *
   * @return array
   *   The build array.
   */
  public function preRender(array $build) {
    // Dispatch event to allow the block to be altered before 'preRender' runs.
    // This is required for proptypes validation and fill up.
    $event = new PdbTwigPrePreRenderEvent($build, $this->configuration, $this->classInstance);
    $this->eventDispatcher->dispatch($event, PdbTwigPrePreRenderEvent::PRE_PRERENDER);

    $build = $event->getBuild();
    $this->configuration = $event->getConfig();

    // If `$build` is empty then do not render the component, ie, return
    // an empty array.
    if (empty($build)) {
      return [];
    }

    if ($this->classInstance && method_exists($this->classInstance, 'view')) {
      $build_view = $this->classInstance->view($this->configuration);

      // Only process if the `view()` returns an array.
      // This accounts for when `view()` returns NULL instead which means
      // do not add anything to `$build` and move forward.
      if (isset($build_view) && is_array($build_view)) {
        if (count($build_view)) {
          $build = NestedArray::mergeDeep($build, $build_view);
        }
        else {
          // If `view()` returned empty array then that means the component
          // must not be rendered, so empty the `$build` variable.
          // Lets keep `#cache` information that will still be needed for
          // caching purposes.
          $build = [
            '#cache' => $build['#cache'] ?? [],
          ];
        }
      }
    }

    // Dispatch event to allow others to alter the block.
    // This is required for proptypes autofill of template variables.
    // This sounds like a more useful event overall though.
    $event = new PdbTwigPreRenderAlterEvent($build, $this->configuration, $this->classInstance);
    $this->eventDispatcher->dispatch($event, PdbTwigPreRenderAlterEvent::PRERENDER_ALTER);

    $build = $event->getBuild();

    return $build;
  }

  /**
   * Retrieves the Component class instance.
   *
   * @return object|null
   *   The Component class instance or null.
   */
  protected function getClassInstance() {
    $info = $this->getComponentInfo();

    if (isset($info['class']) && method_exists($info['class'], 'create')) {
      $class_instance = $info['class']::create(\Drupal::getContainer());
      return $class_instance;
    }

    return NULL;
  }

  /**
   * Retrieves the Component block cache defaults.
   *
   * @return array
   *   The cache defaults.
   */
  protected function getCacheDefaults() {
    $defaults = [
      'keys' => [
        'pdb',
        'pdb_twig',
        $this->configuration['id'],
      ],
      'max-age' => -1,
      'tags' => [
        'twig_component',
        $this->configuration['id'],
      ],
      'contexts' => [],
    ];

    return $defaults;
  }

}
