<?php

namespace Drupal\pdb_twig\Plugin\Derivative;

use Drupal\pdb\Plugin\Derivative\PdbBlockDeriver;
use Drupal\pdb_twig\Plugin\Context\TwigContextDefinition;

/**
 * Derives block plugin definitions for Twig components.
 */
class TwigBlockDeriver extends PdbBlockDeriver {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = parent::getDerivativeDefinitions($base_plugin_definition);

    $twig_definitions = array_filter($definitions, function (array $definition) {
      return $definition['info']['presentation'] === 'twig';
    });

    // Some extra stuff for twig components.
    foreach ($twig_definitions as $block_id => $definition) {
      $status = $definition['info']['status'] ?? 'active';

      // Add some support to hide the block on the UI.
      // The block will be created but not listed on the block layout UI.
      if ($status === 'hidden') {
        $twig_definitions[$block_id]['_block_ui_hidden'] = TRUE;

        // Create a non-existing context so the block will be hidden other UIs.
        if (empty($definition['context_definitions'])) {
          $twig_definitions[$block_id]['context_definitions'] = [];
        }

        // Set a defualt value so it will not fail when creating instances
        // with the component manager service.
        $context_def = new TwigContextDefinition('pdb:hidden');
        $context_def->setDefaultValue('hidden');

        $twig_definitions[$block_id]['context_definitions']['pdb_hidden'] = $context_def;
      }
    }

    return $twig_definitions;
  }

}
