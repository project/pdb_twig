<?php

namespace Drupal\pdb_twig;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Utility\ProjectInfo;
use Drupal\pdb\ComponentDiscoveryInterface;

/**
 * Discovery service for component locale translations.
 */
class ComponentTranslationsDiscovery implements ComponentTranslationsDiscoveryInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The component discovery service.
   *
   * @var \Drupal\pdb\ComponentDiscoveryInterface
   */
  protected $componentDiscovery;

  /**
   * Constructs a new ComponentTranslationsDiscovery.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\pdb\ComponentDiscoveryInterface $component_discovery
   *   The component discovery service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ComponentDiscoveryInterface $component_discovery) {
    $this->moduleHandler = $module_handler;
    $this->componentDiscovery = $component_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentTranslations() {
    $components = $this->componentDiscovery->getComponents();

    // Get the components that provide locale translations.
    // Follows locale module approach to discover translation projects.
    // @see locale_translation_project_list().
    $projects = [];
    $additional_whitelist = [
      'interface translation project',
      'interface translation server pattern',
    ];

    $this->moduleHandler->loadInclude('locale', 'inc', 'locale.compare');
    $component_data = _locale_translation_prepare_project_list($components, 'pdb');

    // Add the `status` property required for projects.
    // This is required because this is using `ProjectInfo` for processing.
    foreach ($component_data as $index => $component) {
      $component_data[$index]->status = TRUE;
    }

    $project_info = new ProjectInfo();
    $project_info->processInfoList($projects, $component_data, 'pdb', TRUE, $additional_whitelist);

    return $projects;
  }

}
