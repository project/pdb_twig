<?php

namespace Drupal\pdb_twig;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a service with support for twig components.
 */
class ComponentManager implements ComponentManagerInterface {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The contexts required by plugins being built.
   *
   * @var array
   */
  protected $contexts = [];

  /**
   * Constructs a new ComponentManager.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The ContextHandler for applying contexts to conditions properly.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(BlockManagerInterface $block_manager, ContextHandlerInterface $context_handler, RequestStack $request_stack) {
    $this->blockManager = $block_manager;
    $this->contextHandler = $context_handler;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function build($component, array $config = []) {
    $block_plugin = $this->blockManager->createInstance("twig_component:$component", $config);

    // Check and build block required contexts.
    $context_defs = $block_plugin->getContextDefinitions();
    if ($context_defs) {
      $request = $this->requestStack->getCurrentRequest();

      foreach ($context_defs as $context_key => $context_def) {
        // Do not process pdb:hidden context definitions.
        if ($context_def->getDataType() === 'pdb:hidden') {
          continue;
        }

        if (!isset($this->contexts[$context_key])) {
          $context_name = $context_def->getDataDefinition()->getEntityTypeId();

          if ($request->attributes->has($context_name)) {
            $value = $request->attributes->get($context_name);
            $this->contexts[$context_key] = new Context($context_def, $value);
          }
        }
      }
      $this->contextHandler->applyContextMapping($block_plugin, $this->contexts);
    }

    $block_build = $block_plugin->build();
    return $block_build;
  }

}
