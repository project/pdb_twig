<?php

namespace Drupal\pdb_twig\Template;

use Drupal\pdb_twig\ComponentManagerInterface;

/**
 * Provides a class to allow dynamic build of twig function per component.
 */
class PdbTwigComponentFunction {

  /**
   * The component associated with the twig function.
   *
   * @var string
   */
  protected $component;

  /**
   * The pdb twig component manager service.
   *
   * @var \Drupal\pdb_twig\ComponentManagerInterface
   */
  protected $componentManager;

  /**
   * Constructs a new PdbTwigComponentFunction.
   *
   * @param string $component
   *   The component associated with the twig function.
   * @param \Drupal\pdb_twig\ComponentManagerInterface $component_manager
   *   The pdb twig component manager service.
   */
  public function __construct($component, ComponentManagerInterface $component_manager) {
    $this->component = $component;
    $this->componentManager = $component_manager;
  }

  /**
   * Builds a twig component using the twig component manager service.
   *
   * @param array $config
   *   (Optional) The component config.
   *
   * @return array
   *   The render array for the twig component.
   */
  public function build($config = []) {
    $block_build = $this->componentManager->build($this->component, $config);
    return $block_build;
  }

}
