<?php

namespace Drupal\pdb_twig\Template;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\pdb\ComponentDiscoveryInterface;
use Drupal\pdb_twig\ComponentManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides a twig function for each pdb twig component.
 */
class PdbTwigComponentExtension extends AbstractExtension {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The component discovery service.
   *
   * @var \Drupal\pdb\ComponentDiscoveryInterface
   */
  protected $componentDiscovery;

  /**
   * The pdb twig component manager service.
   *
   * @var \Drupal\pdb_twig\ComponentManagerInterface
   */
  protected $componentManager;

  /**
   * Constructs a new PdbTwigComponentExtension.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\pdb\ComponentDiscoveryInterface $component_discovery
   *   The component discovery service.
   * @param \Drupal\pdb_twig\ComponentManagerInterface $component_manager
   *   The pdb twig component manager service.
   */
  public function __construct(
    CacheBackendInterface $cache,
    ComponentDiscoveryInterface $component_discovery,
    ComponentManagerInterface $component_manager
  ) {
    $this->cacheBackend = $cache;
    $this->componentDiscovery = $component_discovery;
    $this->componentManager = $component_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    $functions = [];

    $components = $this->getComponents();
    foreach ($components as $component_name => $component) {
      if ($component->info['presentation'] !== 'twig') {
        continue;
      }

      // Instanciate a PdbTwigComponentFunction with the right component.
      // This way the component can be dynamic without requiring a new
      // method for each one.
      $component_function = new PdbTwigComponentFunction($component_name, $this->componentManager);
      $functions[] = new TwigFunction($component_name, [$component_function, 'build']);
    };

    return $functions;
  }

  /**
   * Get components from component discovery service.
   *
   * Add a cache layer as this is executed on every request that is not cached.
   *
   * @return array
   *   Associative array with components data.
   */
  protected function getComponents(): array {
    // No need for cache tags as this will require a full cache clear for
    // other reasons like derivatives, hook theme, etc.
    $cid = 'pdb_twig:pdb_twig_component_extension:components';

    if ($cache = $this->cacheBackend->get($cid)) {
      return $cache->data;
    }

    $components = $this->componentDiscovery->getComponents();

    $this->cacheBackend->set($cid, $components, Cache::PERMANENT);

    return $components;
  }

}
