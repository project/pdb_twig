<?php

namespace Drupal\pdb_twig;

/**
 * Defines the interface a service that discovers component locale translations.
 */
interface ComponentTranslationsDiscoveryInterface {

  /**
   * Find all components providing translations.
   *
   * @return array
   *   The components providing translations.
   */
  public function getComponentTranslations();

}
