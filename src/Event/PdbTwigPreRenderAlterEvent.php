<?php

namespace Drupal\pdb_twig\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Defines an event for TwigBlock `preRender` alter.
 */
class PdbTwigPreRenderAlterEvent extends Event {

  /**
   * Name of the event fired to alter `preRender`.
   */
  const PRERENDER_ALTER = 'pdb_twig.prerender_alter';

  /**
   * The block build data.
   *
   * @var array
   */
  protected $build;

  /**
   * The block config.
   *
   * @var array
   */
  protected $config;

  /**
   * The Component class instance.
   *
   * @var object
   */
  protected $classInstance;

  /**
   * Constructs a new PdbTwigPreRenderAlterEvent.
   *
   * @param array $build
   *   The block build data.
   * @param array $config
   *   The block config.
   * @param object $class_instance
   *   The Component class instance.
   */
  public function __construct(array $build, array $config, $class_instance) {
    $this->config = $config;
    $this->build = $build;
    $this->classInstance = $class_instance;
  }

  /**
   * Get the block build data.
   *
   * @return array
   *   The stored block build data.
   */
  public function getBuild() {
    return $this->build;
  }

  /**
   * Set the block build data.
   *
   * @param array $build
   *   The block build data to store.
   */
  public function setBuild(array $build) {
    return $this->build = $build;
  }

  /**
   * Get the block config.
   *
   * @return array
   *   The stored block config.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Set the block config.
   *
   * @param array $config
   *   The block config to store.
   */
  public function setConfig(array $config) {
    return $this->config = $config;
  }

  /**
   * Get the class instance.
   *
   * @return object
   *   The stored class instance.
   */
  public function getClassInstance() {
    return $this->classInstance;
  }

  /**
   * Set the class instance.
   *
   * @param object $class_instance
   *   The class instance to store.
   */
  public function setClassInstance($class_instance) {
    return $this->classInstance = $class_instance;
  }

}
