<?php

namespace Drupal\pdb_twig\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Provides an event to customize component themes.
 */
class PdbTwigThemeEvent extends Event {

  /**
   * Name of the PDB theme items event.
   */
  const THEME = 'pdb_twig.theme';

  /**
   * Component theme items.
   *
   * @var array
   */
  protected $items;

  /**
   * Constructs a new PdbTwigThemeEvent.
   *
   * @param array $items
   *   The theme items.
   */
  public function __construct(array $items) {
    $this->items = $items;
  }

  /**
   * Get the items.
   *
   * @return array
   *   The stored theme items.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Set the items.
   *
   * @param array $items
   *   Theme items to store.
   */
  public function setItems(array $items) {
    return $this->items = $items;
  }

}
