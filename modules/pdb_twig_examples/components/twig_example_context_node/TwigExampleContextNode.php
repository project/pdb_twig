<?php

namespace Drupal\pdb_twig_examples\components\twig_example_context_node;

use Drupal\pdb_twig_examples\components\ComponentBase;

/**
 * Provides custom build steps for the twig-example-context-node twig block.
 */
class TwigExampleContextNode extends ComponentBase {

  /**
   * {@inheritdoc}
   */
  public function view(array $config) {
    $build = [];

    // Do not render if no context is provided.
    if (!isset($config['contexts']['entity:node'])) {
      return $build;
    }

    $node = $config['contexts']['entity:node'];

    $build['#content'] = [
      'title' => $node->label(),
    ];

    return $build;
  }

}
