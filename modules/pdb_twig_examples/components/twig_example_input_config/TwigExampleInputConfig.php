<?php

namespace Drupal\pdb_twig_examples\components\twig_example_input_config;

use Drupal\pdb_twig_examples\components\ComponentBase;

/**
 * Provides custom build steps for the twig-example-input-config twig block.
 */
class TwigExampleInputConfig extends ComponentBase {

  /**
   * {@inheritdoc}
   */
  public function view(array $config) {
    $build = [];

    $content = $config['content'] ?? [];

    $build['#content'] = [
      'title' => $content['title'] ?? '',
      'description' => $content['description'] ?? '',
    ];

    return $build;
  }

}
