<?php

namespace Drupal\pdb_twig_examples\components\twig_example_class;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pdb_twig_examples\components\ComponentBase;

/**
 * Provides custom build steps for the twig-example-class twig block.
 */
class TwigExampleClass extends ComponentBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function view(array $config) {
    $build = [];

    $build['#content'] = [
      'title' => $this->t('Example Class'),
      'description' => $this->t('This shows a component using a PHP class.'),
    ];

    return $build;
  }

}
