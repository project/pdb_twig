<?php

namespace Drupal\pdb_twig_examples\components;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides basic logic for a Component class.
 */
class ComponentBase {

  /**
   * Creates a new instance of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return object
   *   The new class instance.
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

}
